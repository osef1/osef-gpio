cmake_minimum_required(VERSION 3.13)

include(osef.cmake)

project(osef-gpio)

add_subdirectory(osef-posix/File)

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME}
  PRIVATE
    SysGPIO/SysGPIO.cpp
    SysGPIO/SysInputGPIO.cpp
    SysGPIO/SysOutputGPIO.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
        SysGPIO
)

target_link_libraries(${PROJECT_NAME}
    osef-file
)