#!/bin/bash

mkdir -p build/debug

cd build/debug

# [core] C/C++ general-purpose checkers
# [cplusplus] C++ specific checkers
# [deadcode] dead code checkers
# [optin] portability checkers
# [security] security checkers
# [unix] POSIX/Unix checkers
# [valist] variable arguments checkers
declare -a SCAN_BUILD_ENABLE_ARRAY=("core" "cplusplus" "deadcode" "optin" "security" "unix" "valist")
for i in "${SCAN_BUILD_ENABLE_ARRAY[@]}"
do
   SCAN_BUILD_ENABLE+="-enable-checker "$i" "
   SCAN_BUILD_ENABLE_LIST+=$i" "
done

# [deadcode.DeadStores] forbids to include shared_ptr_atomic.h included by thread
# [nullability] Objective C checkers
# [osx] Apple OSX OS checkers
# [fuchsia] Google Fuchsia OS checkers
declare -a SCAN_BUILD_DISABLE_ARRAY=("deadcode.DeadStores" "nullability" "osx" "fuchsia")
for i in "${SCAN_BUILD_DISABLE_ARRAY[@]}"
do
   SCAN_BUILD_DISABLE+="-disable-checker "$i" "
   SCAN_BUILD_DISABLE_LIST+=$i" "
done

echo scan-build enabled checkers: $SCAN_BUILD_ENABLE_LIST

echo scan-build disabled checkers: $SCAN_BUILD_DISABLE_LIST

scan-build $SCAN_BUILD_ENABLE $SCAN_BUILD_DISABLE -v --show-description --status-bugs -analyze-headers -o . --exclude googletest-src/ cmake -DCMAKE_BUILD_TYPE=debug ../..

scan-build $SCAN_BUILD_ENABLE $SCAN_BUILD_DISABLE -v --show-description --status-bugs -analyze-headers -o . --exclude googletest-src/ make
