#include "DevGPIO.h"
#include "Debug.h"

#include <fcntl.h>  // open
#include <sys/ioctl.h>  // ioctl
#include <unistd.h>  // close
#include <string.h>
#include <linux/gpio.h>  // memset

//    #include <errno.h>
//    #include <gpiod.h>
//    #include <limits.h>
//    #include <sys/stat.h>
//    #include <sys/sysmacros.h>
//    #include <sys/types.h>

OSEF::DevGPIO::DevGPIO(const uint32_t& chipId)
    :chip(chipId),
    filename("/dev/gpiochip"+std::to_string(chipId)),
    fd(-1),
    request({{0}, 0, {0}, {0}, 0, -1})
{
//    std::string name = "";
//    std::string label = "";
//    uint32_t lines;
//    if (getChipInfo(name, label, lines))
//    {
//        std::string name = "";
//        std::string consumer = "";
//        uint32_t flags;
//        for(uint32_t i=0; i<lines; i++)
//        {
//            getLineInfo(i, name, consumer, flags);
//        }
//    }
}

OSEF::DevGPIO::~DevGPIO()
{
    if (fd != -1)
    {
        close(fd);
    }
}

bool OSEF::DevGPIO::openFd()
{
    bool ret = true;

    if (fd == -1)
    {
        fd = open(filename.c_str(), O_RDWR);
        if (fd <= 0)
        {
            fd = -1;
            ret = false;
        }
    }

    return ret;
}

bool OSEF::DevGPIO::getChipInfo(std::string& name, std::string& label, uint32_t& lines)
{
    bool ret = false;

    if (openFd())
    {
        struct gpiochip_info info;
        if (ioctl(fd, GPIO_GET_CHIPINFO_IOCTL, &info) == 0)
        {
            name = info.name;
            label = info.label;
            lines = info.lines;
            DOUT("Character device chip name:" << name << " label:" << label << " lines:" << lines);
            ret = true;
        }
    }

    return ret;
}

bool OSEF::DevGPIO::getLineInfo(const uint32_t& line, std::string& name, std::string& consumer, uint32_t& flags)
{
    bool ret = false;

    if (openFd())
    {
        struct gpioline_info info;
        memset(&info, 0, sizeof(info));
        info.line_offset = line;
        if (ioctl(fd, GPIO_GET_LINEINFO_IOCTL, &info) == 0)
        {
            name = info.name;
            consumer = info.consumer;
            flags = info.flags;
            DOUT("Character device " << chip << " line " << line << " name:" << name << " consumer:" << consumer << " flags:" << flags);
            ret = true;
        }
    }

    return ret;
}

bool OSEF::DevGPIO::getValue(const uint32_t& line, bool& val)
{
    bool ret = false;

    if (openFd())
    {
        struct gpiohandle_request req;
        req.lineoffsets[0] = line;
        req.lines = 1;
        req.flags = GPIOHANDLE_REQUEST_INPUT;
//        strcpy(req.consumer_label, "DevGPIO");
        snprintf(req.consumer_label, sizeof(req.consumer_label), "DevGPIO");
        if (ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &req) == 0)
        {
            struct gpiohandle_data data;
            memset(&data, 0, sizeof(data));
            if (ioctl(req.fd, GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data) == 0)
            {
                if (data.values[0] == 0)
                {
                    val = false;
                }
                else
                {
                    val = true;
                }
                DOUT("Character device chip:" << chip << " line:" << line << " level:" << val);
                ret = true;
            }
        }
    }

    return ret;
}
