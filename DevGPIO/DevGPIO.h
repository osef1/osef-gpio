#ifndef OSEFDEVGPIO_H
#define OSEFDEVGPIO_H

#include <linux/gpio.h>  // gpiohandle_request
#include <string>

namespace OSEF
{
    class DevGPIO
    {
    public:
        explicit DevGPIO(const uint32_t& chipId);
        virtual ~DevGPIO();

        bool getChipInfo(std::string& name, std::string& label, uint32_t& lines);
        bool getLineInfo(const uint32_t& line, std::string& name, std::string& consumer, uint32_t& flags);
        bool getValue(const uint32_t& line, bool& val);

    private:
        DevGPIO(const DevGPIO& orig);
        DevGPIO& operator=(const DevGPIO& orig);

        bool openFd();
        bool requestInput();

        uint32_t chip;
        std::string filename;
        int32_t fd;
        gpiohandle_request request;
    };
}  // namespace OSEF

#endif /* OSEFDEVGPIO_H */
