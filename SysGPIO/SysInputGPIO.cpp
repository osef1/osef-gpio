#include "SysInputGPIO.h"
#include "OverwriteFile.h"
#include "ReadFile.h"
#include "Debug.h"

#include <fcntl.h>  // open
#include <unistd.h>  // lseek
#include <poll.h>  // pollfd

OSEF::SysInputGPIO::SysInputGPIO(const uint32_t& p)
    :SysGPIO(p),
    edgeFilename(PIN_PATH + "/edge"),
    activeLevelFilename(PIN_PATH + "/active_low")
{
    if (not setDirectionIn())
    {
        DERR("error setting direction IN");
    }
}

bool OSEF::SysInputGPIO::setDirectionIn() const
{
    const bool ret = OverwriteFile::overwriteString(getDirectionFilename(), "in");
    return ret;
}

bool OSEF::SysInputGPIO::setEdgeNone() const
{
    const bool ret = OverwriteFile::overwriteString(edgeFilename, "none");
    return ret;
}

bool OSEF::SysInputGPIO::setEdgeRising() const
{
    const bool ret = OverwriteFile::overwriteString(edgeFilename, "rising");
    return ret;
}

bool OSEF::SysInputGPIO::setEdgeFalling() const
{
    const bool ret = OverwriteFile::overwriteString(edgeFilename, "falling");
    return ret;
}

bool OSEF::SysInputGPIO::setEdgeBoth() const
{
    const bool ret = OverwriteFile::overwriteString(edgeFilename, "both");
    return ret;
}

bool OSEF::SysInputGPIO::setActiveLevelLow() const
{
    const bool ret = OverwriteFile::overwriteString(activeLevelFilename, "1");
    return ret;
}

bool OSEF::SysInputGPIO::setActiveLevelHigh() const
{
    const bool ret = OverwriteFile::overwriteString(activeLevelFilename, "0");
    return ret;
}

bool OSEF::SysInputGPIO::waitForInterrupt() const
{
    bool ret = false;

    const int32_t fd = open(getValueFilename().c_str(), GPIO_O_RDONLY);  // open file
    if (fd >= 0)
    {
        if (lseek(fd, 0, GPIO_SEEK_SET) >= 0L)
        {
            char buf = 0;
            if (read(fd, &buf, 1U) == 1)
            {
                struct pollfd fds[1];
                fds[0].fd       = fd;
                fds[0].events   = GPIO_POLLPRI|GPIO_POLLERR;
                fds[0].revents  = 0;

                if (poll(&fds[0], 1U, -1) == 1)
                {
                    ret = true;
                }
            }
            else
            {
                DERR("error read file");
            }
        }
        else
        {
            DERR("error lseek file");
        }

        if (close(fd) != 0)
        {
            DERR("error closing file");
        }
    }
    else
    {
        DERR("error open file");
    }

    return ret;
}

bool OSEF::SysInputGPIO::getEdge(std::string& e) const
{
    bool ret = false;

    const std::string fs = edgeFilename;
    ret = ReadFile::readLine(fs, e);

    return ret;
}

bool OSEF::SysInputGPIO::getActiveLevel(std::string& lvl) const
{
    bool ret = false;

    const std::string fs = activeLevelFilename;
    ret = ReadFile::readLine(fs, lvl);

    return ret;
}
