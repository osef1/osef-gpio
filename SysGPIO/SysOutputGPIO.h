#ifndef OSEFSYSOUTPUTGPIO_H
#define OSEFSYSOUTPUTGPIO_H

#include "SysGPIO.h"

namespace OSEF
{
    class SysOutputGPIO : public SysGPIO
    {
    public:
        explicit SysOutputGPIO(const uint32_t& p);
        SysOutputGPIO(const uint32_t& p, const bool& level);
        ~SysOutputGPIO() override = default;

        bool setHigh() const;
        bool setLow() const;

        SysOutputGPIO(const SysOutputGPIO&) = delete;  // copy constructor
        SysOutputGPIO& operator=(const SysOutputGPIO&) = delete;  // copy assignment
        SysOutputGPIO(SysOutputGPIO&&) = delete;  // move constructor
        SysOutputGPIO& operator=(SysOutputGPIO&&) = delete;  // move assignment

    private:
        bool setDirectionOut() const;
        bool setDirectionLow() const;
        bool setDirectionHigh() const;
    };
}  // namespace OSEF

#endif /* OSEFSYSOUTPUTGPIO_H */
