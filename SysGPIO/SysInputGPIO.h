#ifndef OSEFSYSINPUTGPIO_H
#define OSEFSYSINPUTGPIO_H

#include "SysGPIO.h"

#include <string>

namespace OSEF
{
    class SysInputGPIO : public SysGPIO
    {
    public:
        explicit SysInputGPIO(const uint32_t& p);
        ~SysInputGPIO() override = default;

        bool getEdge(std::string& e) const;
        bool setEdgeNone() const;
        bool setEdgeRising() const;
        bool setEdgeFalling() const;
        bool setEdgeBoth() const;

        bool getActiveLevel(std::string& lvl) const;
        bool setActiveLevelLow() const;
        bool setActiveLevelHigh() const;

        bool waitForInterrupt() const;

        SysInputGPIO(const SysInputGPIO&) = delete;  // copy constructor
        SysInputGPIO& operator=(const SysInputGPIO&) = delete;  // copy assignment
        SysInputGPIO(SysInputGPIO&&) = delete;  // move constructor
        SysInputGPIO& operator=(SysInputGPIO&&) = delete;  // move assignment

    private:
        bool setDirectionIn() const;

        const std::string edgeFilename;
        const std::string activeLevelFilename;
    };
}  // namespace OSEF

#endif /* OSEFSYSINPUTGPIO_H */
