#include "SysGPIO.h"
#include "OverwriteFile.h"
#include "ReadFile.h"
#include "Debug.h"

#include <fcntl.h>  // open
#include <unistd.h>  // lseek
#include <sys/stat.h>  // stat

OSEF::SysGPIO::SysGPIO(const uint32_t& p)
    :pin(p),
    exportFilename(GPIO_PATH + "/export"),
    unexportFilename(GPIO_PATH + "/unexport"),
    directionFilename(PIN_PATH + "/direction"),
    valueFilename(PIN_PATH + "/value")
{
    if (not exportPin())
    {
        DERR("error exporting pin");
    }
}

OSEF::SysGPIO::~SysGPIO()
{
    if (not unexportPin())
    {
        DERR("error unexporting pin");
    }
}

bool OSEF::SysGPIO::isExported() const
{
    bool ret = false;

    struct stat sb{};
    if (stat(std::string(PIN_PATH).c_str(), &sb) == 0)
    {
        ret = true;
    }

    return ret;
}

bool OSEF::SysGPIO::exportPin() const
{
    bool ret = true;

    if (not isExported())
    {
        ret = OverwriteFile::overwriteString(exportFilename, std::to_string(pin));
    }

    return ret;
}

bool OSEF::SysGPIO::unexportPin() const
{
    bool ret = true;

    if (isExported())
    {
        ret = OverwriteFile::overwriteString(unexportFilename, std::to_string(pin));
    }

    return ret;
}

bool OSEF::SysGPIO::getValue(bool& v) const
{
    bool ret = false;

    std::string s;
    if (ReadFile::readLine(valueFilename, s))
    {
        if (s == "0")
        {
            v = false;
            ret = true;
        }
        else if (s == "1")
        {
            v = true;
            ret = true;
        }
        else
        {
            ret = false;  // to avoid empty else statement
            DERR("error reading value");
        }
    }

    return ret;
}

bool OSEF::SysGPIO::getDirection(std::string& d) const
{
    const bool ret = ReadFile::readLine(directionFilename, d);
    return ret;
}
