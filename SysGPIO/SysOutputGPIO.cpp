#include "SysOutputGPIO.h"
#include "OverwriteFile.h"
#include "Debug.h"

OSEF::SysOutputGPIO::SysOutputGPIO(const uint32_t& p)
    :SysGPIO(p)
{
    if (not setDirectionOut())
    {
        DERR("error setting direction OUT");
    }
}

OSEF::SysOutputGPIO::SysOutputGPIO(const uint32_t& p, const bool& level)
    :SysGPIO(p)
{
    if (level)
    {
        if (not setDirectionHigh())
        {
            DERR("error setting direction HIGH");
        }
    }
    else
    {
        if (not setDirectionLow())
        {
            DERR("error setting direction LOW");
        }
    }
}

bool OSEF::SysOutputGPIO::setDirectionOut() const
{
    const bool ret = OverwriteFile::overwriteString(getDirectionFilename(), "out");
    return ret;
}

bool OSEF::SysOutputGPIO::setDirectionLow() const  // set direction out with initialization at low value
{
    const bool ret = OverwriteFile::overwriteString(getDirectionFilename(), "low");
    return ret;
}

bool OSEF::SysOutputGPIO::setDirectionHigh() const  // set direction out with initialization at high value
{
    const bool ret = OverwriteFile::overwriteString(getDirectionFilename(), "high");
    return ret;
}

bool OSEF::SysOutputGPIO::setHigh() const
{
    const bool ret = OverwriteFile::overwriteString(getValueFilename(), "1");
    return ret;
}

bool OSEF::SysOutputGPIO::setLow() const
{
    const bool ret = OverwriteFile::overwriteString(getValueFilename(), "0");
    return ret;
}
