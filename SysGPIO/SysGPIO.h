#ifndef OSEFSYSGPIO_H
#define OSEFSYSGPIO_H

#include <cstdint>  // uint8_t
#include <string>

#ifndef PIN_PATH
    #define PIN_PATH (GPIO_PATH + "/gpio" + std::to_string(getPin()))
#else
    #error PIN-PATH already defined
#endif

namespace OSEF
{
    const std::string GPIO_PATH = "/sys/class/gpio";
    const size_t GPIO_MAX_TEXT_SIZE = 8U;

    const int32_t GPIO_O_RDONLY = 0;  // O_RDONLY
    const int32_t GPIO_SEEK_SET = 0;  // SEEK_SET

    const uint16_t GPIO_POLLIN = 1U;    // #define POLLIN   0x001 /* There is data to read.  */
    const uint16_t GPIO_POLLPRI = 2U;   // #define POLLPRI  0x002 /* There is urgent data to read.  */
    const uint16_t GPIO_POLLERR = 8U;   // #define POLLERR  0x008 /* Error condition.  */

    class SysGPIO
    {
    public:
        explicit SysGPIO(const uint32_t& p);
        virtual ~SysGPIO();

        uint32_t getPin() const {return pin;}

        bool getValue(bool& v) const;
        bool getDirection(std::string& d) const;

        std::string getDirectionFilename() const {return directionFilename;}
        std::string getValueFilename() const {return valueFilename;}

        SysGPIO(const SysGPIO&) = delete;  // copy constructor
        SysGPIO& operator=(const SysGPIO&) = delete;  // copy assignment
        SysGPIO(SysGPIO&&) = delete;  // move constructor
        SysGPIO& operator=(SysGPIO&&) = delete;  // move assignment

    private:
        const uint32_t pin;
        const std::string exportFilename;
        const std::string unexportFilename;
        const std::string directionFilename;
        const std::string valueFilename;

        bool isExported() const;
        bool exportPin() const;
        bool unexportPin() const;
    };
}  // namespace OSEF

#endif /* OSEFSYSGPIO_H */
