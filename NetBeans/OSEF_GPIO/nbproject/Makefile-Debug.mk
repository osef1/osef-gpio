#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/ef6abba4/DevGPIO.o \
	${OBJECTDIR}/_ext/2adeda5c/SysGPIO.o \
	${OBJECTDIR}/_ext/2adeda5c/SysInputGPIO.o \
	${OBJECTDIR}/_ext/2adeda5c/SysOutputGPIO.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_gpio.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_gpio.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_gpio.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_gpio.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_gpio.a

${OBJECTDIR}/_ext/ef6abba4/DevGPIO.o: ../../DevGPIO/DevGPIO.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/ef6abba4
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-posix/File -I../../osef-posix/osef-log/Debug -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ef6abba4/DevGPIO.o ../../DevGPIO/DevGPIO.cpp

${OBJECTDIR}/_ext/2adeda5c/SysGPIO.o: ../../SysGPIO/SysGPIO.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/2adeda5c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-posix/File -I../../osef-posix/osef-log/Debug -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2adeda5c/SysGPIO.o ../../SysGPIO/SysGPIO.cpp

${OBJECTDIR}/_ext/2adeda5c/SysInputGPIO.o: ../../SysGPIO/SysInputGPIO.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/2adeda5c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-posix/File -I../../osef-posix/osef-log/Debug -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2adeda5c/SysInputGPIO.o ../../SysGPIO/SysInputGPIO.cpp

${OBJECTDIR}/_ext/2adeda5c/SysOutputGPIO.o: ../../SysGPIO/SysOutputGPIO.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/2adeda5c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-posix/File -I../../osef-posix/osef-log/Debug -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2adeda5c/SysOutputGPIO.o ../../SysGPIO/SysOutputGPIO.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
